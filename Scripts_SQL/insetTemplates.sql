drop table wp_projetTut_templatesCV_titre;
drop table wp_projetTut_templatesCV_experiences;
drop table wp_projetTut_templatesCV_competences;
drop table wp_projetTut_templatesCV_formations;
drop table wp_projetTut_templatesCV_contact;
drop table wp_projetTut_templatesCV_presentation;
drop table wp_projetTut_templatesCV_situation;



CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_titre (id INT(20) PRIMARY KEY, nom VARCHAR(30));
CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_experiences (id INT(20) PRIMARY KEY, moisDeb VARCHAR(4), moisFin VARCHAR(4), annee VARCHAR(4), ville VARCHAR(20), nomEnt VARCHAR(30), secteur VARCHAR(30), poste VARCHAR(50), mission VARCHAR(200));
CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_competences (id INT(20) PRIMARY KEY);
CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_formations (id INT(20) PRIMARY KEY, annee VARCHAR(4), ville VARCHAR(20), diplome VARCHAR(30), univ VARCHAR(30));
CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_contact (id INT(20) PRIMARY KEY);
CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_presentation (id INT(20) PRIMARY KEY);
CREATE TABLE IF NOT EXISTS wp_projetTut_templatesCV_situation (id INT(20) PRIMARY KEY);


insert into wp_projetTut_templatesCV_titre VALUES(1,"Exemple Titre");
insert into wp_projetTut_templatesCV_experiences VALUES(1,"oct.","jui.","2012","orleans","axerial","informatique","eplucheur de pomme de terre"," ma mission au sein de cette entreprise consisté à prendre une patate ainsi qu'un économe, puis à délicatement prélever la peau de la pomme de terre");
insert into wp_projetTut_templatesCV_competences VALUES(1);
insert into wp_projetTut_templatesCV_formations VALUES(1,"2013","orleans","bac","iut informatique d'orleans");
insert into wp_projetTut_templatesCV_contact VALUES(1);
insert into wp_projetTut_templatesCV_presentation VALUES(1);
insert into wp_projetTut_templatesCV_situation VALUES(1);
