DROP TABLE wp_projetTut_destinataire;

CREATE TABLE IF NOT EXISTS wp_projetTut_destinataire
	( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, id_user INT(6) UNSIGNED,
	nom VARCHAR(20), prenom VARCHAR(20), civilite INT(1),
	entreprise VARCHAR(50), adresse_postale VARCHAR(100), CP INT(5),
 	ville VARCHAR(30), adresse_mail VARCHAR(50) );

insert into wp_projetTut_destinataire values (1,1,"Lecomte","Clément",0,"Yolo","365 rue de la rue",18000,"Olivet","adresse@mail.fr"); 
insert into wp_projetTut_destinataire values (2,1,"Laurent","Remi",0,"Remi Corporation","365 rue du paysan",45160,"Olivet","RemiLaurant@mail.fr");   
