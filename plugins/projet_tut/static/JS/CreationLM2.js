

function gererAccord(){
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
  }
  gererAccord()

function updatePDF(){
  preview_container.attr('src', pdf.output('datauristring'));
}

function creationHeadInformation(nom,prenom,age,adressPostal,codePostal,ville,tel,email){
  pdf.text(10, 10, ""+nom+" "+prenom);
  pdf.text(10, 15, ""+age+" ans");
  pdf.text(10, 20, ""+adressPostal);
  pdf.text(10, 25, ""+codePostal+" "+ville);
  pdf.text(10, 30, "Tél : "+tel);
  pdf.text(10, 35, "Email : "+email);
  updatePDF();
}

function creationHeadDestinataire(entreprise,nom,prenom,adresse_postale,cp,ville,civil){
  pdf.text(150, 40, ""+entreprise);
  console.log(civil);
  if(civil = "0"){
    pdf.text(150, 45, "Monsieur "+nom+" "+prenom);
  }
  else{
    pdf.text(150, 45, "Madame "+nom+" "+prenom);
  }
  pdf.text(150, 50, ""+adresse_postale);
  pdf.text(150, 55, ""+cp+" "+ville);
  pdf.text(208, 60, "|");
  updatePDF();
}


function ajouterParagraphe() {
  var titreParagraphe = document.getElementById("textTitreParagraphe").value;
  if(titreParagraphe.length!=0){
    var max = 0;
    var listeBoite = document.getElementsByClassName("groupe");
    for(var i = 0 ; i<listeBoite.length;i++){
      if(listeBoite[i].id > max){
        max = listeBoite[i].id;
      }
    }
    creationBoite(parseInt(max)+1,titreParagraphe );
    document.getElementById("textTitreParagraphe").value = "";
    gererAccord()
  }
  else{
    window.alert("Remplir le nm du parapgraphe");
  }
}

  function creationBoite(indice,title){
    var ajouterText = "\
    <div id='"+indice+"'class='groupe'>\
      <button id='accord' class='accordion'>Paragraphe : "+title+"</button>\
      <div id='panel"+indice+"' class='panel echerche"+title+"'><p class='textePara' id='texte"+indice+"'></p>\
        <div class='col-md-12'>\
        <button type='button' class='btn btn-primary descendre' onclick='descendre("+indice+")' id='d' name='descende'>Descende</button>";

        if(indice==1){
          ajouterText+="<button type='button' display='none' class='btn btn-primary monter' onclick='monter("+indice+")' id='m' name='monter'>Monter</button>";
        }
        else{
          ajouterText+="<button type='button'  class='btn btn-primary monter' onclick='monter("+indice+")' id='m' name='monter'>Monter</button>";
        }
        ajouterText+="\
        </div>\
        <div class='col-md-6'>\
          <input class='form-control texteModif' type='text' name='modificationText' id='textModif''/>\
        </div>\
        <div class='col-md-6'>\
          <button class='bouttonModif btn btn-warning' type='button' onclick='modification("+indice+")' id='modif' name='modifier'>Modifier</button>\
          <button class='boutonSupp btn btn-danger' type='button' onclick='supprimer("+indice+")' id='suppr' name='supprimer'>Supprimer paragraphe</button>\
        </div>\
      </div>\
    </div>";
      $( ".accord" ).append(ajouterText);
  }

  function descendre(id){
    texteSelection = document.getElementById("texte"+id).innerHTML;
    texteChanger = document.getElementById("texte"+(id+1)).innerHTML;
    document.getElementById("texte"+id).innerHTML = texteChanger;
    document.getElementById("texte"+(id+1)).innerHTML = texteSelection;
  }

  function monter(id){
    texteSelection = document.getElementById("texte"+id).innerHTML;
    texteChanger = document.getElementById("texte"+(id-1)).innerHTML;
    document.getElementById("texte"+id).innerHTML = texteChanger;
    document.getElementById("texte"+(id-1)).innerHTML = texteSelection;
  }

  function modification(indice)
  {
    var groupes = document.getElementsByClassName("groupe");
    var boite = null;
    for(var i = 0 ; i<groupes.length;i++){
      if(groupes[i].id == indice){
        boite = groupes[i]
      }
    }
    var valeurModifier = boite.getElementById("textTitreParagraphe");
    var text = boite.getElementById("texte");
    if(valeurModifier.value.length != 0){
      text.innerHTML = valeurModifier.value;
      valeurModifier.value = "";
      pdf.text(35, 25, valeurModifier);
      updatePDF();

    }
  }



  function donnerIndice(){
    var taille = document.getElementsByClassName("accordion").length;
    var listeB = document.getElementsByClassName("accordion");
    var listeD = document.getElementsByClassName("panel");
    for(var i = 0; i<taille;i++){
      var indice=i+1;
      var boutton = listeB[i];
      boutton.id = 'accord'+indice;

      var panel = listeD[i];
      panel.id = "panel"+indice;



      var descendre = panel.getElementsByClassName('descendre')[0];
      descendre.id = "d"+indice;
      descendre.name = "descendre"+indice;
      descendre.onclick.value = function onclick(event){ descende(indice); } ;

      var texteM = panel.getElementsByClassName('texteModif')[0];
      texteM.name = "modificationText"+indice;
      texteM.id = "textModif"+indice;

      var bMotif = panel.getElementsByClassName('bouttonModif')[0];
      bMotif.onclick = function onclick(event) { modification(indice); } ;
      bMotif.id = "modif"+indice;
      bMotif.name = "modifier"+indice;

      var bSupp = panel.getElementsByClassName('boutonSupp')[0];
      bSupp.id = "suppr"+indice;
      bSupp.name = "supprimer"+indice;
      document.getElementById("suppr"+indice).onclick = function onclick(event) { supprimer(indice); };

      var textPara = panel.getElementsByClassName('textePara')[0];
      textPara.id = "texte"+indice;


    }

  }

  function validation(){
    // création du PDF
    pdf.save('test1.pdf');
  }


  function supprimer(nom)
  {
    console.log(nom);
    var boutton = document.getElementsByClassName("recherche"+nom)[0];
    boutton.parentNode.removeChild(boutton);

    var div = document.getElementsByClassName("recherche"+nom)[0];
    div.parentNode.removeChild(div);
    donnerIndice();
  }
