
$( document ).ready(function() {
  var url = window.location.search;
  var id_design = url.substring(url.indexOf("=")+1);
  console.log($(id_design).selector);

  if ($(id_design).selector == "0") {
    var ajouter = document.getElementsByClassName("ajouter");
    $(ajouter)[0].className = "ajouter btn btn-warning btn-lg btn-block disabled";
    var verifier = document.getElementsByClassName("verifier");
    $(verifier)[0].className = "verifier btn btn-success btn-lg btn-block glyphicon glyphicon-ok disabled";
  }
  else {
    var design = document.getElementsByClassName("design");
    $(design)[0].className = "design glyphicon glyphicon-pencil btn btn-primary btn-lg btn-block disabled";
  }
});
