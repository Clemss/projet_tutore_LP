<?php

//initialisation
require_once __DIR__.'/vendor/autoload.php';
$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), array(
	'twig.path' => __DIR__.'/templates'
));
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array(
		'driver' => 'pdo_mysql',
		'dbhost' => '192.168.82.168',
		'dbname' => 'dblenoir',
		'user' => 'lenoir',
		'password' => 'oui salut',
	),
));
$app->register(new Silex\Provider\SessionServiceProvider());
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
try {$app['twig']->addGlobal('session', $_SESSION);} catch(Exception $e) {$app['twig']->addGlobal('session', null);}
$app['debug'] = true;
include("config.php");
$app['twig']->addGlobal('lien_WP', $lien_WP);
$app['twig']->addGlobal('lien_PHP', $lien_PHP);

//vérification de la connexion pour acceder au site
$app->before(function (Request $request) use ($app) {
	set_admin_role();
	if ( stristr($request->getRequestUri(), "projet_tut.php/connexion") ) {
		//ne rien faire si on accede au truc de connexion :)
	}
	else {
		//si on accede à un autre truc,
		include("config.php");
		$user = $app['session']->get('user');
		if ($user == null) {
			//et qu'on est pas connecté, alors on redirige
			return $app->redirect($lien_PHP."/projet_tut.php/connexion");
		}
	}
});


$app->get('/', function () use ($BD_host, $BD_database, $BD_user, $BD_passwd, $app, $BD_prefix) {
	//detection des tables
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$reponse = $bdd->query("SHOW TABLES LIKE '".$BD_prefix."projetTut_userFriends';");
	$installed = false;
	while($donnees = $reponse->fetch()) {
		$installed = true;
	}
	$user_id = getUserid($app);
	if ( ! $installed) {
		return $app['twig']->render('home_popup_BD.html', array("connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
	}
	else if (!userHaveInformations($user_id)) { //informations utilisateur manquantes
		$username = $app['session']->get('user')['username'];
		$valeurs = getUserInformations($user_id);
		return $app['twig']->render('home_popup_userInfo.html', array("user" => $app['session']->get('user')['username'], "valeurs" => $valeurs, "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
	}
	else {
		return $app['twig']->render('home.html', array("user" => $app['session']->get('user')['username'], "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
	}
});

$app->post('/set_user_info', function(Request $request) use($app) {
	include("config.php");
	$user = $app['session']->get('user');
	foreach ($_POST as $param) {
		if (strstr($param, "--")) {
			echo "caractères '--' invalide, merci de pas tenter d'injection, la bise <br><br>Tata";
			die();
		}
		if (strstr($param, "\"")) {
			echo "caractère '\"' invalide, merci de pas tenter d'injection, la bise <br><br>Tata";
			die();
		}
	}
	$inputNom = $_POST['inputNom'];
	$inputPrenom = $_POST['inputPrenom'];
	$inputAge = $_POST['inputAge'];
	$inputAdresse = $_POST['inputAdresse'];
	$inputCP = $_POST['inputCP'];
	$inputVille = $_POST['inputVille'];
	$inputTel = $_POST['inputTel'];
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$requ = $bdd->query("select * from ".$BD_prefix."projetTut_informations where user_id=".getUserid($app).";");
	if ($requ == false || empty($requ->fetch())) {
		$requ = $bdd->query("insert into ".$BD_prefix."projetTut_informations(user_id, nom, prenom, age, adresse_postal, code_postal, ville, numero_tel, role, permis)
			values(
			'".getUserid($app)."',
			'".$inputNom."',
			'".$inputPrenom."',
			'".$inputAge."',
			'".$inputAdresse."',
			'".$inputCP."',
			'".$inputVille."',
			'".$inputTel."',
			'eleve',
			'0');");
		echo "insert into ".$BD_prefix."projetTut_informations(user_id, nom, prenom, age, adresse_postal, code_postal, ville, numero_tel, role, permis)
			values(
			'".getUserid($app)."',
			'".$inputNom."',
			'".$inputPrenom."',
			'".$inputAge."',
			'".$inputAdresse."',
			'".$inputCP."',
			'".$inputVille."',
			'".$inputTel."',
			'eleve',
			'0');";
	}
	else {
		$requ = $bdd->query("UPDATE ".$BD_prefix."projetTut_informations
			SET nom='".$inputNom."',
			prenom='".$inputPrenom."',
			age='".$inputAge."',
			adresse_postal='".$inputAdresse."',
			code_postal='".$inputCP."',
			ville='".$inputVille."',
			numero_tel='".$inputTel."'
			WHERE user_id=".getUserid($app).";");
	}
	return $app->redirect($lien_PHP."/projet_tut.php/");
});


// Template pour la création d'une lettre de motivation
$app->get('/creation2LM/{id_dest}', function ($id_dest) use ($app) {
	$username = $app['session']->get('user')['username'];
	$valeurs = getUserInformations(getUseridFromUsername($username));
	$dest = donneesDestinataire($id_dest,$username);
	return $app['twig']->render('CreationLM2.html', array("titre" => "Creation Lettre de motivation", "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin","valeur"=>$valeurs,"dest"=>$dest));
});

// // Fonction qui sauvegarde la lettre de motivation dans la BDD
// $app->get('/creationLmBdd', function() use ($app,$lien_PHP) {
// 	insertionPargraphe($listeParagraphe);
//
//
// 	return $app->redirect($lien_PHP."/projet_tut.php/fichiers");
// });
//
// insertionPargraphe($listeParagraphe,$id){
// 	for($paragraphe in $listeParagraphe){
//
// 	}
// }



$app->get('/creation2LM/{dest}', function ($dest) use ($app) {
	$liste_dest = donneesDestinataire($dest);
	$liste_user = getUserInformations();

	return $app['twig']->render('CreationLM2.html', array("titre" => "Creation Lettre de motivation", "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin", "informationDestinataire"=>$liste_dest,"informationUser"=>$liste_user));
});

$app->get('/creationCV2', function () use ($app) {
	return $app['twig']->render('creationCV2.html', array("titre" => "Création CV", "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/creationCV2/{cv}', function ($cv) use ($app) {
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
	die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$username = $app['session']->get('user')['username'];
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");
	$id = $reponse->fetch()[0];
	$valeur = getUserInformations(getUseridFromUsername($username));
	$reponse3 = $bdd->query("select * from wp_projetTut_templatesCV_titre where id = ".$id.";");
	if ( $reponse3 ==false ) {
		return "Pas d'information pour l'ID titre cv";
	}
	$titrecv = $reponse3->fetch();
	$reponse4 = $bdd->query("select * from wp_projetTut_templatesCV_experiences where id = ".$id.";");
	if ( $reponse4 ==false ) {
		return "Pas d'information pour l'ID experiences";
	}
	$experiences = $reponse4->fetch();
	$reponse5 = $bdd->query("select * from wp_projetTut_templatesCV_formations where id = ".$id.";");
	if ( $reponse5 ==false ) {
		return "Pas d'information pour l'ID formations";
	}
	$formations = $reponse5->fetch();
	$reponse6 = $bdd->query("select * from wp_projetTut_templatesCV_contact where id = ".$id.";");
	if ( $reponse6 ==false ) {
		return "Pas d'information pour l'ID contact";
	}
	$contact = $reponse6->fetch();
	$reponse7 = $bdd->query("select * from wp_projetTut_templatesCV_presentation where id = ".$id.";");
	if ( $reponse7 ==false ) {
		return "Pas d'information pour l'ID presentation";
	}
	$presentation = $reponse7->fetch();
	$reponse8 = $bdd->query("select * from wp_projetTut_templatesCV_situation where id = ".$id.";");
	if ( $reponse8 ==false ) {
		return "Pas d'information pour l'ID situation";
	}
	$situation = $reponse8->fetch();
	return $app['twig']->render('creationCV2.html', array("titre" => "Création CV", "cv" => $cv, "valeur"=>$valeur, "titrecv" => $titrecv, "experiences" => $experiences, "formations" => $formations, "contact" => $contact, "presentation" => $pressentation, "situation" => $situation, "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/choixTemplatesCV/{cv}', function ($cv) use ($app) {
	return $app['twig']->render('choixTemplatesCV.html', array( "cv" => $cv, "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});


// -- Gestion des fichiers d'un utilisateur
$app->get('/fichiers', function () use ($app) {
	$username = $app['session']->get('user')['username'];
	$fichier = get_fichiers($username);

	return $app['twig']->render('fichiers.html', array("titre" => "Liste des fichiers", "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin", "fichiers" => $fichier));
});

$app->get('/telechargement_file/{fichier}', function ($fichier) use ($app,$lien_PHP) {

	// Création des headers, pour indiquer au navigateur qu'il s'agit d'un fichier à télécharger
		// readfile($lien_PHP."/upload/".$fichier);
		// return $lien_PHP;
		header('Content-Transfer-Encoding: binary'); //Transfert en binaire (fichier)
	  header('Content-Disposition: attachment; filename='.$fichier); //Nom du fichier
	  header('Content-Length: ' . filesize($lien_PHP."/upload"."/".$fichier)); //Taille du fichier
		readfile($lien_PHP."/upload"."/".$fichier);
	});





$app->get('/ajoutContenu', function () use ($app) {
	return $app['twig']->render('ajouter_contenu.html', array("connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/display', function () use ($app) {
	return $app['twig']->render('lmDisplay.html', array("connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/CVvalidation/{id}', function ($id) use ($app) {
	$uidFichier = getInfoFichier($id, 'uid');

	$paragraphes = getParagraphesFichier($id);

	return $app['twig']->render('CVvalidation.html',array(
		"titre" => "Validation CV",
		"listePara" =>$paragraphes,
		"connecte" => true,
		"fichier" => $uidFichier,
		"admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/LMvalidation/{id}', function ($id) use ($app) {
	return "oui";
});

$app->get('/LMvalidation/{id}', function ($id) use ($app) {
	$listeParagraphe=array('Sujet','Entreprise','Motivation');
	return $app['twig']->render('LMvalidation.html',array("titre" => "Validation Lettre de motivation","listePara"=>$listeParagraphe, "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/entreprises', function () use ($app) {
	$username = $app['session']->get('user')['username'];
	$liste_destinaraires=get_list_Destinataires($username);
	return $app['twig']->render('entreprises.html', array("titre" => "Liste des contacts", "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin", "destinations" => $liste_destinaraires));
});

$app->get('/details_entreprises/{id}', function ($id) use ($app) {
	$username = $app['session']->get('user')['username'];
	$info_dest = donneesDestinataire($id,$username);
	return $app['twig']->render('details_entreprises.html', array("connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin","destinataire"=>$info_dest, "id"=>$id));
});

//Page de modification en base de donnée pour une entreprise
$app->get('/modification_entreprises/{id}', function ($id) use ($app) {
	include("config.php");

	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur lors de la modification des informations</h1></center> <br><br>' . $e->getMessage());}
		$bdd->query("update wp_projetTut_destinataire set
		entreprise='".$_GET["tNomEnt"]."',
		nom='".$_GET["tNomD"]."',
		civilite=".$_GET["CivD"].",
		adresse_postale='".$_GET["tAdrPostD"]."',
		CP=".$_GET["tCPD"].",
		ville='".$_GET["tVilleD"]."',
		adresse_mail='".$_GET["mAddMailD"].
 		"' where id = ".$id.";");
		return $app->redirect($lien_PHP."/projet_tut.php/details_entreprises/".$id);
});
$app->get('/remove_entreprises/{id}/{pro}', function ($id,$pro) use ($app,$lien_PHP) {
	remove_destinataire($pro,$id);
	return $app->redirect($lien_PHP."/projet_tut.php/entreprises");
});

$app->get('/informations', function () use ($app) {
	$username = $app['session']->get('user')['username'];
	$valeurs = getUserInformations(getUseridFromUsername($username));

	return $app['twig']->render('informations.html', array("connecte" => true, "valeurs" => $valeurs, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/creationCV1', function () use ($app) {

	$username = $app['session']->get('user')['username'];

	$valeurs = getUserInformations(getUseridFromUsername($username));
	return $app['twig']->render('creationCV1.html', array("connecte" => true,"valeurs"=>$valeurs, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/affichage_lm_cv/{id_fichier}', function ($id_fichier) use ($app) {
	$username = $app['session']->get('user')['username'];
	$file = getUserFileById($id_fichier,$username);
	return $app['twig']->render('afficher_lm_cv.html', array("titre" => "Consultation de votre Document", "connecte" => true,"admin" => getRole($app['session']->get('user')['username']) === "admin","fichier"=>$file));
});

$app->get('/creationLM1', function () use ($app) {

		$username = $app['session']->get('user')['username'];

		$valeurs = getUserInformations(getUseridFromUsername($username));
		$liste_destinaraires=get_list_Destinataires($username);
		// return $liste_destinaraires;
		return $app['twig']->render('creationLM1.html', array("connecte" => true,"valeurs"=>$valeurs, "admin" => getRole($app['session']->get('user')['username']) === "admin","destinations" => $liste_destinaraires));
});


// Creation d'un Destinataire à partir de la page de LM1
$app->get('/creationDestinataire/{nom}/{prenom}/{civilite}/{entreprise}/{adresse_postale}/{CP}/{ville}/{adresse_mail}', function ($nom, $prenom, $civilite,$entreprise, $adresse_postale, $CP,$ville, $adresse_mail) use ($app,$lien_PHP) {
	// return $adresse_mail;
	$id = creationDestinataireBDD($nom, $prenom, $civilite,$entreprise, $adresse_postale, $CP,$ville, $adresse_mail,$app);
	// return $id;
	return $app->redirect($lien_PHP."/projet_tut.php/creation2LM/".$id);
});

// Creation d'un Destinataire à partir de la page entreprise
$app->get('/creationDestinataireEnt/{nom}/{prenom}/{civilite}/{entreprise}/{adresse_postale}/{CP}/{ville}/{adresse_mail}', function ($nom, $prenom, $civilite,$entreprise, $adresse_postale, $CP,$ville, $adresse_mail) use ($app,$lien_PHP) {
	// return $adresse_mail;
	$id = creationDestinataireBDD($nom, $prenom, $civilite,$entreprise, $adresse_postale, $CP,$ville, $adresse_mail,$app);
	// return $id;
	return $app->redirect($lien_PHP."/projet_tut.php/entreprises");
});

// Fonction qui insert le nouveau destinataire en BDD et retoun l'id

function creationDestinataireBDD($nom, $prenom, $civilite,$entreprise, $adresse_postale, $CP,$ville, $adresse_mail,$app){
	include("config.php");

	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}

	$username = $app['session']->get('user')['username'];
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");
	$id = $reponse->fetch()[0];

	$req = $bdd->exec("insert into ".$BD_prefix."projetTut_destinataire (id_user,nom, prenom, civilite,entreprise, adresse_postale, CP,ville, adresse_mail )
	values (".$id.", '".$nom."', '".$prenom."', ".$civilite.", '".$entreprise."', '".$adresse_postale."', ".$CP.", '".$ville."', '".$adresse_mail."');");
	if ($req === false) {
		die('erreur  '."insert into ".$BD_prefix."projetTut_destinataire (id_user,nom, prenom, civilite,entreprise, adresse_postale, CP,ville, adresse_mail )
		values (".$id.", ".$nom.", ".$prenom.", ".$civilite.", ".$entreprise.", ".$adresse_postale.", ".$CP.", ".$ville.", ".$adresse_mail.");");
	}
	$reponse2 = $bdd->query("select * from ".$BD_prefix."projetTut_destinataire where id_user = ".$id." and nom= '".$nom."' and entreprise= '".$entreprise."';");
	if ($reponse2 === false) {
		die('erreur  '."select * from ".$BD_prefix."projetTut_destinataire where id_user = '".$id."' and nom= '".$nom."' and entreprise= '".$entreprise."';");
	}

	while($donnees = $reponse2->fetch()) {
		$id_destination = $donnees[0];
	}

	// return "select * from ".$BD_prefix."projetTut_destinataire where id_user = ".$id." and nom= '".$nom."' and entreprise= '".$entreprise."';";

	return $id_destination;

}

$app->get('/affichageDestinataire', function ($id) use ($app) {
	return $app['twig']->render('affichageDestinataire.html', array("connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin"));
});

$app->get('/remove_file/{pro}/{id}/{fichier}', function ($pro,$id,$fichier) use ($app, $lien_PHP) {
	remove_file($pro,$id);
	// return $lien_PHP."/upload"."/".$fichier;
	if(file_exists("./upload/".$fichier)){
		if(@unlink("./upload/".$fichier))
			return $app->redirect($lien_PHP."/projet_tut.php/fichiers");
		}
	else {
		return "noooopppp  ".$lien_PHP."/upload"."/".$fichier."\n".$lien_PHP."/upload"."/".$fichier;
	}
	return $app->redirect($lien_PHP."/projet_tut.php/fichiers");
});

/**
* créé la base au premier lancement
*/
$app->get('/create_database', function () use ($BD_host, $BD_database, $BD_user, $BD_passwd, $app, $BD_prefix, $lien_PHP) {
	create_database($BD_host, $BD_database, $BD_user, $BD_passwd, $BD_prefix);
	return $app->redirect($lien_PHP."/projet_tut.php");
});

function create_database($BD_host, $BD_database, $BD_user, $BD_passwd, $BD_prefix) {
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('Erreur : ' . $e->getMessage());}


	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_userFriends ( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, user1_id INT(6) UNSIGNED, user2_id INT(6) UNSIGNED );");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_userFriends');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_comments ( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, user_id INT(6) UNSIGNED, file_id INT(6) UNSIGNED, texte TEXT, date DATETIME );");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_comments');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_files ( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, path TEXT,	name TEXT, date DATETIME, scheduled_date DATETIME, type VARCHAR(2), proprio INT(6));");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_files');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_appreciations ( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, id_file INT(6) UNSIGNED, texte TEXT, start_char INT(4), end_char INT(4) );");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_appreciations');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_contents ( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, id_file INT(6) UNSIGNED, type VARCHAR(2), titre VARCHAR(250), texte TEXT, number INT(2), valide BOOLEAN, appreciation TEXT );");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_contents');
	}

	//(id[0-999 999],id_user[0-999 999],nom[a-z]{0-20},prenom[a-z]{0-20},
	//civilite[0-1], entreprise[]{0-50},adresse_Postale[]{0-100}, code_postal[0-9]{5},
	//ville[a-z]{0-30}, adresse_mail[]{10-50})
	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_destinataire
	( id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, id_user INT(6) UNSIGNED,
	nom VARCHAR(20), prenom VARCHAR(20), civilite INT(1),
	entreprise VARCHAR(50), adresse_postale VARCHAR(100), CP INT(5),
 	ville VARCHAR(30), adresse_mail VARCHAR(50) );");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_destinataire');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_informations (ID_infor BIGINT(20) PRIMARY KEY, user_id INT(6) UNSIGNED, nom VARCHAR(20), prenom VARCHAR(20) , age INT(3), adresse_postal VARCHAR(50),code_postal INT(5),ville VARCHAR(30),numero_tel VARCHAR(10), role VARCHAR(30),permis VARCHAR(1));");
	if ($req === false) {
		die('invalid request you dumb shit projetTut_informations');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_titre (id INT(20) PRIMARY KEY, nom VARCHAR(30));");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_templatesCV_titre');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_experiences (id INT(20) PRIMARY KEY, moisDeb VARCHAR(4), moisFin VARCHAR(4), annee VARCHAR(4), ville VARCHAR(20), nomEnt VARCHAR(30), secteur VARCHAR(30), poste VARCHAR(50), mission VARCHAR(200));");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_templatesCV_experiences');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_competences (id INT(20) PRIMARY KEY);");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_templatesCV_competences');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_formations (id INT(20) PRIMARY KEY, annee VARCHAR(4), ville VARCHAR(20), diplome VARCHAR(30), univ VARCHAR(30));");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_templatesCV_formations');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_contact (id INT(20) PRIMARY KEY);");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_templatesCV_contact');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_presentation (id INT(20) PRIMARY KEY);");
	if ($req === false) {
		die('invalid request you dumb shit create projetTut_templatesCV_presentation');
	}

	$req = $bdd->exec("CREATE TABLE IF NOT EXISTS ".$BD_prefix."projetTut_templatesCV_situation (id INT(20) PRIMARY KEY);");
	if ($req === false) {
		die('invalid request pour create projetTut_templatesCV_situation');
	}
}

/**
* reset la base de données à la demande d'un admin
*/
$app->get('/reset_database', function () use ($app) {
	//si appelé par un admin, reset les tables du plugin
	if( getRole($app['session']->get('user')['username']) == "admin") {


		//tout droplimoges

		include("config.php");
		try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
		catch (Exception $e) {die('Erreur : ' . $e->getMessage());}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_userRole;");



		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_userFriends;");
		if ($req === false) {
			die('invalid request you dumb shit drop projetTut_userFriends');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_comments;");
		if ($req === false) {
			die('invalid request you dumb shit drop projetTut_comments');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_files;");
		if ($req === false) {
			die('invalid request you dumb shit drop projetTut_files');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_appreciations;");
		if ($req === false) {
			die('invalid request you dumb shit drop projetTut_appreciations');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_contents;");
		if ($req === false) {
			die('invalid request you dumb shit drop projetTut_contents');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_informations;");
		if ($req === false) {
			die('invalid request pour drop informations');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_titre;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_experiences;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_competences;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_formations;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_contact;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_presentation;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		$req = $bdd->exec("drop table ".$BD_prefix."projetTut_templatesCV_situation;");
		if ($req === false) {
			die('invalid request you dumb shit');
		}

		//tout recreer
		create_database($BD_host, $BD_database, $BD_user, $BD_passwd, $BD_prefix);

		return $app->redirect($lien_PHP."/projet_tut.php/");
	}
});

/**
* ajoute à la base de données des trucs de test
*/
$app->get('/sample_database', function () use ($app) {
	//si appelé par un admin, insere des données d'exemple
	if( getRole($app['session']->get('user')['username']) == "admin") {
		include("config.php");
		try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
		catch (Exception $e) {die('Erreur : ' . $e->getMessage());}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_appreciations values (0, 0, 'c trer b1 bravo', 0, 20);");
		if ($req === false) {
			die('invalid request you dumb shit 1  insert projetTut_appreciations');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_comments values (0, 1,0, 'c pa ouf mer sa pass', '2020-01-01 04:20:00');");
		if ($req === false) {
			die('invalid request you dumb shit insert projetTut_comments');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_contents values (0, 0, 'LM', 'le titre', 'oui c le texte lel', 0, FALSE, '');");
		if ($req === false) {
			die('invalid request you dumb shit insert projetTut_contents');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_files values ( 0, '/dossier/fichier_exemple.pdf', 'oui', '2020-01-01 04:20:00', '2020-01-02 04:20:00', 'CV',1);");
		if ($req === false) {
			die('invalid request you dumb shit insert projetTut_files');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_userFriends values ( 0, 0, 1);");
		if ($req === false) {
			die('invalid request you dumb insert shit projetTut_userFriends');
		}

		// $req = $bdd->exec("insert into ".$BD_prefix."projetTut_informations(ID_infor, user_id, role,nom,prenom,age,adresse_postal,code_postal,ville,numero_tel,permis) values (0, 1, 'admin','Lecomte','Clément',1,'14,champs des coquelico',45160,'Paris-téci','0644232681','n');");
		// if ($req === false) {
		// 	die('invalid request pour insert informations');
		// }


		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_titre values (1, 'Exemple Titre');");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_titre');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_experiences values (1, 'oct.', 'jui.', '2012', 'orleans', 'axerial', 'informatique', 'eplucheur de pomme de terre', 'ma mission au sein de cette entreprise consisté à prendre une patate ainsi qu'un économe, puis à délicatement prélever la peau de la pomme de terre');");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_experiences');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_competences values (0, 1);");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_competences');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_formations values (0, 1, '2013','orleans','bac','iut informatique d'orleans');");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_formations');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_contact values (0, 1);");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_contact');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_presentation values (0, 1);");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_presentation');
		}

		$req = $bdd->exec("insert into ".$BD_prefix."projetTut_templatesCV_situation values (0, 1);");
		if ($req === false) {
			die('invalid request you dumb shit projetTut_templatesCV_situation');
		}

		return $app->redirect($lien_PHP."/projet_tut.php/");
	}
});


/**
* système de connexion via la bd de wordpress, gère l'auth
*/
$app->get('/connexion', function (Request $request) use ($app) {
	include("config.php");
	$user = $app['session']->get('user');
	if ($user != null) {
		return $app->redirect($lien_PHP."/projet_tut.php/");
	}

	return $app['twig']->render('connexion.html');
});

$app->post('/connexion', function(Request $request) use($app) {
	include("config.php");
	$user = $app['session']->get('user');
	if ($user != null) {
		return $app->redirect($lien_PHP."/projet_tut.php/");///
	}

	$loginFail = false;
	$username = $request->get('PHP_AUTH_USER', false);
	if($username != "") {
		$password = $request->get('PHP_AUTH_PW');
		if (authentification($username, $password)) {
			$app['session']->set('user', array('username' => $username));

			// si l'utilisateur n'a pas renseigné toutes se informations
			if(!userHaveInformations(getUseridFromUsername($username))){
				//return $app->redirect($lien_PHP."/projet_tut.php/informations");
				//$valeurs = getUserInformations(getUseridFromUsername($username));
				//return $app['twig']->render('informations.html', array("connecte" => true, "valeurs"=>$valeurs, "admin" => getRole($app['session']->get('user')['username']) === "admin", "missed_infomations" => true));
			}
			return $app->redirect($lien_PHP."/projet_tut.php/");///
		}
		$loginFail = true;
	}

	return $app['twig']->render('connexion.html', array("login_fail" => $loginFail, "last_username" => $username));
});

$app->get('/logout', function () use ($app) {
	include("config.php");
	$app['session']->set('user', null);
	return $app->redirect($lien_PHP."/projet_tut.php/");
});

/**
* permet, pour les admins, de modifier les droits des gens
*/
$app->get('/admin_users', function () use ($app) {
	include("config.php");
	if( getRole($app['session']->get('user')['username']) == "admin") {
		$users = get_array_users();
		return $app['twig']->render('user_list.html', array("connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin", "users" => $users));
	}
	else {
		return "touche pas à ça ptit con";
	}
});

$app->get('/set_user_role/{id}', function (Request $request, $id) use ($app) {
	if( getRole($app['session']->get('user')['username']) == "admin") {
		include("config.php");
		$role = $request->query->get('new_role');
		set_role($id, $role);
		return $app->redirect($lien_PHP."/projet_tut.php/admin_users");
	}
	else {
		return "<h1>touche pas à ça ptit con</h1>";
	}
});

$app->get('/display_all', function(Request $request) use ($app) {
	$fichier = get_all_fichiers($username);

	return $app['twig']->render('display_all_docs.html', array("titre" => "Liste des fichiers", "connecte" => true, "admin" => getRole($app['session']->get('user')['username']) === "admin", "fichiers" => $fichier));
});




/**
* enregistre le fichier dans le dossier upload, sous forme {uid}.extensionDOrigine, et enregistre son existence en base
*/
$app->post('/upload', function(Request $request) use ($app) {
	if (isset($_POST['CV'])) {
		$type = "LM";
	}
	else {
		$type = "CV";
	}
	$extension = pathinfo($_FILES['fichier']['name'])['extension'];
	$uid = uniqid();
	$fichier = $uid.".".$extension;
	$uploadfile = getcwd()."/upload/".$fichier;
	//var_dump($uploadfile);
	if (move_uploaded_file($_FILES['fichier']['tmp_name'], $uploadfile)) {
		//echo "fichier enregistré";
	}
	else {
		//echo "erreur fichier";
	}
	//enregistrer en base
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$stmt = $bdd->prepare("insert into ".$BD_prefix."projetTut_files values (0, :path, :name, :date, :date, :type, :proprio)");
	$id = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$app['session']->get('user')['username']."';")->fetch()[0];
	if ($stmt->execute(array("path" => $fichier, "date" => date('Y-m-d H:i:s'), "type" => $type, "name" => $_FILES['fichier']['name'], "proprio" => $id))) {
		//echo("<h1>succès <br><br><br>");
		return $app->redirect($lien_PHP."/projet_tut.php/");
	}
	else {
		//echo("<h1>Erreur BDD<br><br><br>");
	}
	return $app->redirect($lien_PHP."/projet_tut.php/");
});

/**
* modifie un role, appelé par /set_user_role, qui est appelé par /admin_users
*/
function set_role($id, $role) {
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
	die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	if ( getRole_id($id) == "null" ) {
		//user non connu dans la base, ajouter
		$stmt = $bdd->prepare("insert into ".$BD_prefix."projetTut_informations(ID_infor, user_id, role) values (0, :id, :role)");
		echo "<h1>insert</h1> <br><br><br><br><br>";
		if ($stmt->execute(array("role" => $role, "id" => $id))) {
		}
		else {
			die("erreur lors de la modification du role d'un utilisateur (insert)");
		}
	}
	else {
		//user connu dans la base, modifier
		$stmt = $bdd->prepare("update ".$BD_prefix."projetTut_informations set role = :role where user_id = :user_id;");
		echo "<h1>update</h1> <br><br><br><br><br>";
		if ($stmt->execute(array("role" => $role, "user_id" => $id))) {
		}
		else {
			die("erreur lors de la modification du role d'un utilisateur (update)");
		}
	}
}

/**
* retourne le role d'un utilisateur donné
*/

function getRole($username) {
	//retourne le role d'un utilisateur
	//get l'id de l'utilisateur dans la base wordpress
	//et retourne le role correspondant dans la base projetTut_informations
	//retourne "eleve" par defaut si l'id n'existe pas dans la base projetTut_informations
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	try {
		$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");
		$id = $reponse->fetch()[0];
		$reponse = $bdd->query("select role from ".$BD_prefix."projetTut_informations where user_id = '".$id."';");
		if($reponse == false){
			return "null";
		}
		$donnees = $reponse->fetch();
		if ( $donnees == false ) {
			return "null";
		}
		return $donnees[0];
	} catch (Exception $e) {
		return "null";
	}
}

function remove_file($pro,$id){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$reponse = $bdd->query("delete from ".$BD_prefix."projetTut_files where proprio = '".$pro."' AND id ='".$id."';");
}

function remove_destinataire($pro,$id){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$reponse = $bdd->query("delete from ".$BD_prefix."projetTut_destinataire where id = ".$id." AND id_user =".$pro.";");
}
// Affiche les fichiers par ordre croissant
function get_file_by_date(){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");

		$id = $reponse->fetch()[0];
		$reponse2 = $bdd->query("select * from ".$BD_prefix."projetTut_files order by date;");
		if ( $reponse2 ==false ) {
			return "Pas d'information pour l'ID";
		}
		$liste = array();
		while($donnees = $reponse2->fetch()) {
			array_push($liste, array($donnees[0],$donnees[1],$donnees[2],$donnees[3],$donnees[4]));
		}
		return $liste;
}

// Affiche les fichiers par ordre décroissant
function get_file_by_date_desc(){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");

		$id = $reponse->fetch()[0];
		$reponse2 = $bdd->query("select * from ".$BD_prefix."projetTut_files order by date desc;");
		if ( $reponse2 ==false ) {
			return "Pas d'information pour l'ID";
		}
		$liste = array();
		while($donnees = $reponse2->fetch()) {
			array_push($liste, array($donnees[0],$donnees[1],$donnees[2],$donnees[3],$donnees[4]));
		}
		return $liste;
}

// Recherche un fichier à partir de son nom
function search_file($nom){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$reponse = $bdd->query("select * from ".$BD_prefix."projetTut_files where name='".$name."';");
	if ( $reponse == false ) {
		return "Error 404 ! Résultat not found !";
	}
	$liste = array();
	while($donnees = $reponse2->fetch()) {
		array_push($liste, array($donnees[0],$donnees[1],$donnees[2],$donnees[3],$donnees[4]));
	}
	return $liste;
}

function getRole_id($id) {
	//fait pareil que getRole, mais prend un ID au lieu d'un username :)
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$reponse = $bdd->query("select role from ".$BD_prefix."projetTut_informations where user_id = '".$id."';");
	$donnees = $reponse->fetch();
	if ( $donnees == false ) {
		return "null";
	}
	return $donnees[0];
}

/**
* retourne la liste des users de wordpress, sous forme [login, role, id]
*/
function get_array_users() {
	//mysql connexion
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$stmt = $bdd->prepare("select user_login, ID from ".$BD_prefix."users");
	$liste = array();
	if ($stmt->execute()) {
		while($donnees = $stmt->fetch()) {
			$login = $donnees[0];
			array_push($liste, array($login, getRole($login), $donnees[1]));
		}
	}
	else {
		die("<h1>Votre wordpress nest pas installé, ou config.php est mal configuré</h1>");
	}
	return $liste;
}

/**
* vérifie que le login existe et correspond au mot de passe
*/
function authentification($login, $password) {
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$stmt = $bdd->prepare("select user_pass from ".$BD_prefix."users where user_login = :login");
	$stmt->bindParam(':login', $login);
	if ($stmt->execute()) {
		$result = $stmt->fetch(PDO::FETCH_BOTH);
		if (CheckPassword($password, $result[0])) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

/* fonctions wordpress importées salements, ne pas toucher :) */
function CheckPassword($password, $stored_hash)
{
	if ( strlen( $password ) > 4096 ) {
		return false;
	}

	$hash = crypt_private($password, $stored_hash);
	if ($hash[0] == '*')
		$hash = crypt($password, $stored_hash);

	return $hash === $stored_hash;
}

function crypt_private($password, $setting)
{
	$itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	$output = '*0';
	if (substr($setting, 0, 2) == $output)
		$output = '*1';

	$id = substr($setting, 0, 3);
	# We use "$P$", phpBB3 uses "$H$" for the same thing
	if ($id != '$P$' && $id != '$H$')
		return $output;

	$count_log2 = strpos($itoa64, $setting[3]);
	if ($count_log2 < 7 || $count_log2 > 30)
		return $output;

	$count = 1 << $count_log2;

	$salt = substr($setting, 4, 8);
	if (strlen($salt) != 8)
		return $output;

	# We're kind of forced to use MD5 here since it's the only
	# cryptographic primitive available in all versions of PHP
	# currently in use.  To implement our own low-level crypto
	# in PHP would result in much worse performance and
	# consequently in lower iteration counts and hashes that are
	# quicker to crack (by non-PHP code).
	if (PHP_VERSION >= '5') {
		$hash = md5($salt . $password, TRUE);
		do {
			$hash = md5($hash . $password, TRUE);
		} while (--$count);
	} else {
		$hash = pack('H*', md5($salt . $password));
		do {
			$hash = pack('H*', md5($hash . $password));
		} while (--$count);
	}

	$output = substr($setting, 0, 12);
	$output .= encode64($hash, 16);

	return $output;
}

function encode64($input, $count)
{
	$itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	$output = '';
	$i = 0;
	do {
		$value = ord($input[$i++]);
		$output .= $itoa64[$value & 0x3f];
		if ($i < $count)
			$value |= ord($input[$i]) << 8;
		$output .= $itoa64[($value >> 6) & 0x3f];
		if ($i++ >= $count)
			break;
		if ($i < $count)
			$value |= ord($input[$i]) << 16;
		$output .= $itoa64[($value >> 12) & 0x3f];
		if ($i++ >= $count)
			break;
		$output .= $itoa64[($value >> 18) & 0x3f];
	} while ($i < $count);

	return $output;
}

function donneesDestinataire($id_destinataire,$username){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());}
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");
	$id = $reponse->fetch()[0];
	$reponse = $bdd->query("select * from ".$BD_prefix."projetTut_destinataire where id_user = '".$id."' and id='".$id_destinataire."';");
	if ( $reponse ==false ) {
		return "pas de destinataire";
	}
	return $reponse->fetch();

}

/**
 * userHaveInformations
 *
 * Regarde si l'utilisateur a déja renseigné TOUTES ses informations
 *
 * @param (int) (userId) Id de l'utilisateur
 * @return (boolean) Retourn true si l'utilisateur a déja renseigné TOUTES ses informations
 */
function userHaveInformations($userId){
	return !in_array(null, getUserInformations($userId)) && !in_array("", getUserInformations($userId));
}

/**
 * getUserInformations
 *
 * Va chercher les informations de l'utilisateur
 *
 * @param (int) (userId) Id de l'utilisateur
 * @return (array) Retourn les informations de l'utilisateur
 */
function getUserInformations($userId){
	include("config.php");
		$bdd = get_bdd();
		$reponse = $bdd->query("select * from ".$BD_prefix."projetTut_informations where user_id = '".$userId."';");
		$userInformations = array(
			"nom" => null,
			"prenom" => null,
			"age" => null,
			"adressePostal" => null,
			"codePostal" => null,
			"ville" => null,
			"tel" => null,
			//"permis" => null,//TODO: il n'y a pas de champ permis dans la BD
			"email" => null
		);
		if ($reponse) {
			$valeurs = $reponse->fetch();

			$userInformations["nom"] = $valeurs["nom"];
			$userInformations["prenom"] = $valeurs["prenom"];
			$userInformations["age"] = $valeurs["age"];
			$userInformations["adressePostal"] = $valeurs["adresse_postal"];
			$userInformations["codePostal"] = $valeurs["code_postal"];
			$userInformations["ville"] = $valeurs["ville"];
			$userInformations["tel"] = $valeurs["numero_tel"];
		}

		$reponse = $bdd->query("select user_email from ".$BD_prefix."users where ID = '".$userId."';");

		if ($reponse) {
			$valeurs = $reponse->fetch();

			$userInformations["email"] = $valeurs["user_email"];
		}

		return $userInformations;
}

function getUsername($app) {
	return $app['session']->get('user')['username'];
}

function getUserid($app) {
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	if ( getUsername($app) != null ) {
		return $bdd->query("select ID from ".$BD_prefix."users where user_login = '".getUsername($app)."';")->fetch()[0];
	}
	else {
		return false;
	}
}

function getUseridFromUsername($username){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");
    return $reponse->fetch()[0];
}

function get_fichiers($username){

	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");

		$id = $reponse->fetch()[0];
		$reponse2 = $bdd->query("select * from ".$BD_prefix."projetTut_files where proprio='".$id."';");
		if ( $reponse2 ==false ) {
			return "Pas d'information pour l'ID";
		}
		$liste = array();
		while($donnees = $reponse2->fetch()) {
			array_push($liste, array($donnees[0],$donnees[1],$donnees[2],$donnees[3],$donnees[4],$donnees[5],$donnees[6]));
		}
		return $liste;
}

function get_all_fichiers(){

	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}

	$reponse2 = $bdd->query("select * from ".$BD_prefix."projetTut_files;");
	if ( $reponse2 == false ) {
		return "Aucun fichier enregistré";
	}
	$liste = array();
	while($donnees = $reponse2->fetch()) {
		array_push($liste, array($donnees[0],$donnees[1],$donnees[2],$donnees[3],$donnees[4],$donnees[5],$donnees[6]));
	}
	return $liste;
}

function getInfoFichier($id, $val) {
	include("config.php");

	$bdd = get_bdd();

	$reponse = $bdd->query("select * from ".$BD_prefix."projetTut_files where id='".$id."';");

	if($val == "uid") {
		return $reponse->fetch()[1];
	}

	if($val == "name") {
		return $reponse->fetch()[2];
	}

	if($val == "date") {
		return $reponse->fetch()[3];
	}

	if($val == "s_date") {
		return $reponse->fetch()[4];
	}

	if($val == "type") {
		return $reponse->fetch()[5];
	}

	return $reponse->fetch();
}

function getParagraphesFichier($id) {
	include("config.php");

	$bdd = get_bdd();

	$reponse = $bdd->query("select * from ".$BD_prefix."projetTut_contents where id_file='".$id."';");

	$res = array();
	while ($content = $reponse->fetch()) {
		array_push($res, array($content[0], $content[1], $content[2], $content[3], $content[4], $content[5], $content[6]));
	}
	//$res = liste de contents
	//     = [
	//          [id, id_file, type, titre, texte, number, valide],
	//          [id, id_file, type, titre, texte, number, valide], ...
	//       ]
	//pour l'id d'une fichier donné
	return $res;
}

function get_list_Destinataires($username){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$reponse = $bdd->query("select ID from ".$BD_prefix."users where user_login = '".$username."';");
	$id = $reponse->fetch()[0];
	$reponseDest= $bdd->query("select * from ".$BD_prefix."projetTut_destinataire where id_user=".$id.";");
	if($reponseDest == false) {
		return "Pas d'information pour les destinataires";
	}
	$list_destinataires=array();
	while($destinataire = $reponseDest->fetch()) {
		$list_destinataires[$destinataire["id"]]=$destinataire;
	}
	return $list_destinataires;
}
function getUserFileById($id_fichier,$username){
	include("config.php");
	try { $bdd = new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
	$id = getUseridFromUsername($username);
		$reponse = $bdd->query("select * from ".$BD_prefix."projetTut_files where proprio = ".$id." and id = ".$id_fichier." ;");
		$fichier = array(
			"path" => null,
			"name" => null,
			"date" => null,
			"scheduled_date" => null,
			"type" => null,
			"proprio" => null,
			"id" => null
		);
		if ($reponse) {
			$valeurs = $reponse->fetch();

			$fichier["path"] = $valeurs["path"];
			$fichier["name"] = $valeurs["name"];
			$fichier["date"] = $valeurs["date"];
			$fichier["scheduled_date"] = $valeurs["scheduled_date"];
			$fichier["type"] = $valeurs["type"];
			$fichier["proprio"] = $valeurs["proprio"];
			$fichier["id"] = $valeurs["id"];
		}

		return $fichier;
}


function get_bdd(){
	include("config.php");
	try {
		return new PDO('mysql:host='.$BD_host.';dbname='.$BD_database.';charset=utf8', $BD_user, $BD_passwd);
	}
	catch (Exception $e) {
		die('<br><br><center><h1>Erreur de connection à la base de données, veuillez vérifier les paramètres de la base de données dans config.php</h1></center> <br><br>' . $e->getMessage());
	}
}

function set_admin_role() {
	include("config.php");
	$bdd = get_bdd();
	$bdd->query("insert into wp_projetTut_informations(ID_infor, user_id, role) values (0, 1, 'admin');");
	$bdd->query("update wp_projetTut_informations set role='admin' where ID_infor = 0;");
}
$app->run();
