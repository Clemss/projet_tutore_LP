<?php
/*
Plugin Name: CV & LM
Plugin URI: http://example.com
Description: Créer et vérifier ses CV et LM !
Version: 0.10001
Author: LP R&T Orléans 2016-2017
Author URI: https://www.univ-orleans.fr/
License: BeerWare
*/

//lancement du site silex si accès direct, sinon gére les trucs wordpress
if (strstr($_SERVER['REQUEST_URI'],"wp-content")) {
	include_once("app.php");
}

else {
	include_once("wordpress.php");

}
