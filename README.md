# Site web avec CV & LM

[![LE TRELLO](https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Trello-128.png "le trello")](https://trello.com/projetsitecv "le trello")
[![LE DRIVE](http://www.webrankinfo.com/images/google/google-drive-128-128.jpg "le drive")](https://drive.google.com/open?id=0B71HNIWrU-tmSWwzODlNZnZHTlU "le drive")
[![LE DISCORD](https://owkings.com/assets/Discord-Logo-Color-f09e607fef1c62be80e5511fa04555d338a11e736727ce741ce2635e81e3e038.png "le discord")](https://discord.gg/Swcz8 "le discord")
[![NOS MAQUETTES](http://cdn.appstorm.net/web.appstorm.net/files/2012/10/Balsamiq-icon.png "nos maquettes")](https://infoiut45.mybalsamiq.com/projects/cv-motiv/grid "nos maquettes")

## Synopsis

At the top of the file there should be a short introduction and/ or overview that explains **what** the project is. This description should match descriptions added for package managers (Gemspec, package.json, etc.)


## Motivation
Chef de projet, c'est à toi de faire ça :)

## Installation
* Installer un site wordpress sur votre session
    (si besoin, vous pouvez supprimer les enregsitrements BD wordpress avec un script dans tools/)
    (l'ip du serveur de BDD est 192.168.82.168)
* cloner le dépot dans un autre dossier
* créer un lien entre projet/plugins/ et wordpress/wp-content/plugins/
* créer un lien entre projet/themes/{le theme de votre choix} et wordpress/wp-content/themes/
créer un lien :
```
ln -s /le/dossier/source /le/dossier/destination/le_nom_du_lien
```
* créer le fichier config.php en suivant le modele de config_exemple.php
* n'oubliez pas de créer le dossier vendor avec composer.phar install (ne pas le commit)
* créer le dossier upload dans le dossier plugins/projet_tut/ et lui faire un chmod 777

## Tests

## devenir admin:
```
insert into wp_projetTut_informations(ID_infor, user_id, role,nom,prenom,age,adresse_postal,code_postal,ville,numero_tel,permis) values (1, 1, 'admin','ExempleNom','ExemplePrénom',1,'14,champs des coquelico',45160,'Paris-téci','0644895262','n');
```


## Contributors
BRANCHARD Benoit - CAUQUY Jessy - DECOBECQ Alexis - LAURENT Rémi - LECOMTE Clément - LENOIR Antoine - LIMOGES Guillaume

## License
![BeerWare Licence](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/BeerWare_Logo.svg/116px-BeerWare_Logo.svg.png) BeerWare
